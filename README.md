# SportTracker

A simple Android app created to practice Kotlin and Android Jetpack libraries.

![](docs/activity_list.png)

![](docs/insert_activity.png)

## Features
- Store a sport activity with name, location and duration either to local storage or a Firebase backend.
- Sign-up and sign-in provided through Firebase Auth and Google account.
- Autocompletion of existing activities locations when entering a new one.
- Editing existing activities.
- CI/CD pipeline set up to automatically test and build the app on new commits.

TODO:
- [ ] Add option to specify activity location by GPS, or picking a spot on map.
- [ ] Allow the activities to be dragged, sorted and filtered.
- [ ] Write unit and user interface tests.
- [ ] Fix issue where if the activity name is too long, it pushes out the duration icon and text.
- [ ] Provide way to remove existing locations, so they do not appear in the autocomplete list.
- [ ] Display the time when activity was created.

The current .apk file can be downloaded [here](https://dsarman.gitlab.io/C21AllInclusiveCRM/app-release.apk)