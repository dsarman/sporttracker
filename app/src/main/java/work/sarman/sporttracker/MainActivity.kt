package work.sarman.sporttracker

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.drawerlayout.widget.DrawerLayout
import androidx.navigation.NavOptions
import androidx.navigation.NavOptionsBuilder
import androidx.navigation.PopUpToBuilder
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.google.android.material.navigation.NavigationView
import com.google.firebase.auth.FirebaseAuth
import com.mikepenz.aboutlibraries.LibsBuilder
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.nav_header_main.*
import org.koin.android.ext.android.inject


class MainActivity : AppCompatActivity() {

    private val auth by inject<FirebaseAuth>()

    private lateinit var appBarConfiguration: AppBarConfiguration

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val toolbar: Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)

        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        val navView: NavigationView = findViewById(R.id.nav_view)
        val navController = findNavController(R.id.nav_host_fragment)
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        appBarConfiguration = AppBarConfiguration(
            setOf(R.id.nav_activities, R.id.nav_insert_activity), drawerLayout
        )
        setupActionBarWithNavController(navController, appBarConfiguration)
        navView.setupWithNavController(navController)

        navController.addOnDestinationChangedListener { _, destination, _ ->
            if (destination.id != R.id.nav_login) {
                checkLogin()
            }
        }
        setUpNavBarUserInfoRefresh()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.main, menu)
        return true
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when(item.itemId) {
            R.id.action_about -> {
                val libsBuilder = LibsBuilder()
                    .withAboutIconShown(true)
                    .withAboutVersionShown(true)
                    .withAboutDescription("Icons made by Freepik from www.flaticon.com")
                val args = Bundle()
                args.putSerializable("data",libsBuilder)
                val navOptions = NavOptions.Builder().setPopUpTo(R.id.nav_activities, false).build()
                findNavController(R.id.nav_host_fragment).navigate(R.id.nav_about, args, navOptions)
                return true
            }
            else ->super.onOptionsItemSelected(item)
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.nav_host_fragment)
        return navController.navigateUp(appBarConfiguration) || super.onSupportNavigateUp()
    }

    override fun onResume() {
        super.onResume()
        checkLogin()
    }

    /**
     * If the user is not logged-in Firebase Auth UI activity with Google sign-in will be shown
     */
    private fun checkLogin() {
        val user = auth.currentUser
        if (user == null) {
            val navController = findNavController(R.id.nav_host_fragment)
            navController.navigate(R.id.nav_login)
        } else {
            refreshUserInfoInNav()
        }
    }

    private fun setUpNavBarUserInfoRefresh() {
        drawer_layout.addDrawerListener(object : DrawerLayout.DrawerListener {
            override fun onDrawerStateChanged(newState: Int) {
            }

            override fun onDrawerSlide(drawerView: View, slideOffset: Float) {
            }

            override fun onDrawerClosed(drawerView: View) {
            }

            override fun onDrawerOpened(drawerView: View) {
                refreshUserInfoInNav()
            }
        })
    }

    private fun refreshUserInfoInNav() {
        auth.currentUser?.let { user ->
            user.displayName?.let { nav_name_text?.text = it }
            user.email?.let { nav_email_text?.text = it }
        }
    }
}
