package work.sarman.sporttracker.data.db

import androidx.room.TypeConverter
import org.threeten.bp.Duration
import org.threeten.bp.Instant
import work.sarman.sporttracker.data.Storage

/**
 * Used by Room to convert types it does not know.
 */
object Converters {

    @TypeConverter
    @JvmStatic
    fun durationToLong(value: Duration?): Long? = value?.seconds

    @TypeConverter
    @JvmStatic
    fun longToDuration(value: Long?): Duration? = value?.let { Duration.ofSeconds(it) }

    @TypeConverter
    @JvmStatic
    fun instantToTimestamp(value: Instant?): Long? = value?.toEpochMilli()

    @TypeConverter
    @JvmStatic
    fun timestampToInstant(value: Long?): Instant? = value?.let { Instant.ofEpochMilli(it) }

    /**
     * Used to set proper Storage location for all loaded entries when loading straight into DTO
     */
    @TypeConverter
    @JvmStatic
    fun toStorage(@Suppress("UNUSED_PARAMETER") value: Int?): Storage? = Storage.LOCAL
}