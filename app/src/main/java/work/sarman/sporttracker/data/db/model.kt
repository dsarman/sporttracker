package work.sarman.sporttracker.data.db

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey
import org.threeten.bp.Duration
import org.threeten.bp.Instant

@Entity(
    foreignKeys =[
    ForeignKey(
        entity = Location::class,
        parentColumns = ["id"],
        childColumns = ["location_id"],
        onDelete = ForeignKey.SET_NULL
    )]
)
data class SportActivity(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    var id: Long?,
    var name: String,
    var length: Duration,
    var createdAt: Instant,
    @ColumnInfo(name = "location_id")
    var locationId: Long?
)

/**
 * Created to possibly allow adding GPS based locations later.
 */
@Entity
data class Location(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    var id: Long?,
    var name: String
)
