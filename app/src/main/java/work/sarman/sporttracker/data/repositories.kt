package work.sarman.sporttracker.data

import androidx.annotation.WorkerThread
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.Transformations
import androidx.room.withTransaction
import work.sarman.sporttracker.data.db.Location
import work.sarman.sporttracker.data.db.LocationDao
import work.sarman.sporttracker.data.db.SportActivityDao
import work.sarman.sporttracker.data.db.SportTrackerRoomDatabase
import work.sarman.sporttracker.data.firebase.FirebaseDao

class SportActivityRepository(
    private val sportActivityDao: SportActivityDao,
    private val locationRepository: LocationRepository,
    private val firebaseDao: FirebaseDao,
    private val db: SportTrackerRoomDatabase
) {

    private val localActivities = Transformations.distinctUntilChanged(sportActivityDao.getAll())
    private val firebaseActivities = firebaseDao.activities
    private val _allActivities: MediatorLiveData<List<SportActivityDTO>> = MediatorLiveData()
    val allActivities: LiveData<List<SportActivityDTO>> = _allActivities

    init {
        _allActivities.addSource(localActivities) { value ->
            _allActivities.value = combine(value, firebaseActivities.value)
        }

        _allActivities.addSource(firebaseActivities) { value ->
            _allActivities.value = combine(value, localActivities.value)
        }
    }

    /**
     * Combines two given lists into one.
     */
    private fun combine(
        list1: List<SportActivityDTO>?,
        list2: List<SportActivityDTO>?
    ): List<SportActivityDTO> {
        val result = mutableListOf<SportActivityDTO>()
        list1?.let {
            result.addAll(it)
        }
        list2?.let {
            result.addAll(it)
        }
        return result
    }


    @WorkerThread
    suspend fun insert(sportActivityDTO: SportActivityDTO): String =
        when (sportActivityDTO.storage) {
            Storage.LOCAL -> insertLocally(sportActivityDTO).toString()
            Storage.FIREBASE -> {
                val location = sportActivityDTO.getLocationEntity()
                // Insert the location also to local database so it is showed in autocomplete
                locationRepository.upsert(location)

                firebaseDao.upsert(sportActivityDTO)
            }
        }

    /**
     * Inserts given activity DTO into local database.
     */
    private suspend fun insertLocally(sportActivityDTO: SportActivityDTO): Long {
        val activity = sportActivityDTO.getActivityEntity()
        val location = sportActivityDTO.getLocationEntity()
        return db.withTransaction {
            val locationId = locationRepository.upsert(location)
            activity.locationId = locationId
            return@withTransaction sportActivityDao.insert(activity)
        }
    }


    @WorkerThread
    suspend fun update(sportActivityDTO: SportActivityDTO) {
        val location = sportActivityDTO.getLocationEntity()

        db.withTransaction {
            val locationId = locationRepository.upsert(location)
            when (sportActivityDTO.storage) {
                Storage.LOCAL -> {
                    val activity = sportActivityDTO.getActivityEntity()
                    activity.locationId = locationId
                    sportActivityDao.update(activity)
                }
                Storage.FIREBASE -> firebaseDao.upsert(sportActivityDTO)
            }
        }
    }

    @WorkerThread
    suspend fun delete(sportActivityId: String, storage: Storage) {
        when (storage) {
            Storage.LOCAL -> sportActivityDao.deleteById(sportActivityId.toLong())
            Storage.FIREBASE -> firebaseDao.delete(sportActivityId)
        }
    }

    fun refreshFirebaseListener() = firebaseDao.refreshFirebaseListener()
}

class LocationRepository(
    private val locationDao: LocationDao
) {
    fun getAllNames(): LiveData<List<String>> =
        Transformations.distinctUntilChanged(locationDao.getAllNames())

    @WorkerThread
    suspend fun insert(name: String): Long {
        val location = Location(null, name)
        return locationDao.insert(location)
    }

    /**
     * Returns id of given location.
     * If there is no existing location with supplied name, it gets inserted into DB.
     */
    @WorkerThread
    suspend fun upsert(location: Location): Long {
        val locationByName = locationDao.findByName(location.name)
        return if (locationByName != null) {
            locationByName.id!!
        } else {
            locationDao.insert(location)
        }
    }
}