package work.sarman.sporttracker.data.firebase

import org.threeten.bp.Instant

data class FirebaseSportActivity(
    val name: String = "",
    val lengthMillis: Long = 0,
    val createdAtTimestamp: Long = Instant.now().toEpochMilli(),
    val location: String = ""
)