package work.sarman.sporttracker.data.firebase

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import mu.KotlinLogging
import org.threeten.bp.Duration
import org.threeten.bp.Instant
import work.sarman.sporttracker.data.CouldNotGetFirebaseKeyException
import work.sarman.sporttracker.data.SportActivityDTO
import work.sarman.sporttracker.data.Storage
import work.sarman.sporttracker.data.UserNotLoggedInException

private val log = KotlinLogging.logger { }

class FirebaseDao(
    private val auth: FirebaseAuth,
    private val db: FirebaseDatabase
) {
    private val _activities: MutableLiveData<List<SportActivityDTO>> = MutableLiveData(emptyList())
    /**
     * Listener that watches firebase db events for current users activities, and posts the changes
     * into LiveData that is exposed further.
     */
    private val firebaseEventListener =
        object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                val newActivities = mutableListOf<SportActivityDTO>()
                for (child in dataSnapshot.children) {
                    val value = child.getValue(FirebaseSportActivity::class.java) ?: continue
                    val dto = SportActivityDTO(
                        id = child.key,
                        name = value.name,
                        length = Duration.ofMillis(value.lengthMillis),
                        createdAt = Instant.ofEpochMilli(value.createdAtTimestamp),
                        location = value.location,
                        storage = Storage.FIREBASE
                    )
                    newActivities.add(dto)
                    log.debug { "Processing data snapshot child '$child'" }
                }
                _activities.postValue(newActivities)
            }

            override fun onCancelled(e: DatabaseError) {
                log.error { "Could not listen to firebase activities due to error $e" }
            }
        }

    val activities: LiveData<List<SportActivityDTO>> =
        Transformations.distinctUntilChanged(_activities)

    companion object {
        const val ACTIVITIES = "activities"
    }

    init {
        refreshFirebaseListener()
    }

    fun upsert(sportActivityDTO: SportActivityDTO): String {
        val userId = auth.currentUser?.uid ?: throw UserNotLoggedInException()
        val dto = sportActivityDTO.toFirebaseEntity()
        val key = sportActivityDTO.id
            ?: db.reference.child(ACTIVITIES).child(userId).push().key
                ?: throw CouldNotGetFirebaseKeyException()

        db.reference.child(ACTIVITIES).child(userId).child(key).setValue(dto)
        return key
    }


    fun delete(sportActivityId: String) {
        db.reference.child(ACTIVITIES).child(getUserId()).child(sportActivityId).removeValue()
    }

    fun refreshFirebaseListener() {
        val userId: String = try {
            getUserId()
        } catch (e: UserNotLoggedInException) {
            log.warn { "User not logged in when trying to attach Firebase listener" }
            return
        }

        db.reference.child(ACTIVITIES).child(userId).removeEventListener(firebaseEventListener)
        db.reference.child(ACTIVITIES).child(userId).addValueEventListener(firebaseEventListener)
    }

    private fun getUserId() = auth.currentUser?.uid ?: throw UserNotLoggedInException()
}