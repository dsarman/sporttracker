package work.sarman.sporttracker.data

import org.threeten.bp.Duration
import org.threeten.bp.Instant
import work.sarman.sporttracker.data.db.Location
import work.sarman.sporttracker.data.db.SportActivity
import work.sarman.sporttracker.data.firebase.FirebaseSportActivity
import kotlin.math.absoluteValue

data class SportActivityDTO(
    var id: String?,
    val name: String,
    val length: Duration,
    val createdAt: Instant,
    val location: String,
    val storage: Storage
) {
    /**
     * Tries to create local DB entity from the DTO.
     * @throws IllegalArgumentException if called on activity with storage type other than [Storage.LOCAL].
     */
    fun getActivityEntity(): SportActivity {
        if (storage != Storage.LOCAL) {
            throw IllegalArgumentException("Cannot be called with non local stored dto")
        }

        return SportActivity(id?.toLong(), name, length, createdAt, null)
    }

    /**
     * Return location DB entity for the DTO.
     */
    fun getLocationEntity(): Location = Location(null, location)

    /**
     * Tries to return a Firebase database entity from the DTO.
     * @throws IllegalArgumentException if called on activity with storage type other than [Storage.FIREBASE]
     */
    fun toFirebaseEntity(): FirebaseSportActivity {
        if (storage != Storage.FIREBASE) {
            throw IllegalArgumentException("Cannot be called with non locally stored dto")
        }
        return FirebaseSportActivity(
            name, length.toMillis(), createdAt.toEpochMilli(), location
        )
    }

    /**
     * Formats the activity length into a string with hours, minutes and seconds
     */
    fun displayLength(): String {
        val (hours, _) = afterDisplayHours()
        val hoursText = if (hours > 0) {
            "$hours h"
        } else {
            ""
        }

        val (minutes, remainingDuration) = afterDisplayMinutes()
        val minutesText = if (minutes > 0) {
            "$minutes m"
        } else {
            ""
        }

        val seconds = remainingDuration.seconds
        val secondsText = if (seconds > 0) {
            "$seconds s"
        } else {
            ""
        }

        return "$hoursText $minutesText $secondsText".trim()
    }

    /**
     * Returns number of hours and the remaining duration after substracting these hours.
     */
    private fun afterDisplayHours(): Pair<Int, Duration> {
        var tmpDuration = Duration.ofNanos(length.toNanos())
        val hours = tmpDuration.toHours()
        return if (hours >= 1) {
            tmpDuration = tmpDuration.minusHours(hours.absoluteValue)
            hours.absoluteValue.toInt() to tmpDuration
        } else {
            0 to tmpDuration
        }
    }

    /**
     * Returns number of minutes and the duration after substracting already displayed hours and minutes.
     */
    private fun afterDisplayMinutes(): Pair<Int, Duration> {
        var (_, remainingDuration) = afterDisplayHours()

        val minutes = remainingDuration.toMinutes()
        if (minutes > 0) {
            remainingDuration = remainingDuration.minusMinutes(minutes)
        }
        return minutes.toInt() to remainingDuration
    }

    fun getLengthDisplayHours(): Int {
        return afterDisplayHours().first
    }

    fun getLengthDisplayMinutes(): Int {
        return afterDisplayMinutes().first
    }

    fun getLengthDisplaySeconds(): Int {
        val (_, remainingDuration) = afterDisplayMinutes()
        return remainingDuration.seconds.toInt()
    }
}

enum class Storage {
    LOCAL,
    FIREBASE
}