package work.sarman.sporttracker.data.db

import androidx.lifecycle.LiveData
import androidx.room.*
import work.sarman.sporttracker.data.SportActivityDTO

@Dao
interface SportActivityDao {
    @Insert
    suspend fun insert(sportActivity: SportActivity): Long

    @Update
    suspend fun update(sportActivity: SportActivity)

    @Delete
    suspend fun delete(sportActivity: SportActivity)

    @Query("DELETE FROM SportActivity WHERE id = :sportActivityId")
    suspend fun deleteById(sportActivityId: Long)

    @Query(
        """SELECT sa.id, sa.name, sa.length, sa.createdAt, l.name as location, null as storage 
            FROM SportActivity sa JOIN Location l ON sa.location_id = l.id"""
    )
    fun getAll(): LiveData<List<SportActivityDTO>>
}

@Dao
interface LocationDao {
    @Insert
    suspend fun insert(location: Location): Long

    @Update
    suspend fun update(location: Location)

    @Query("SELECT * FROM location WHERE name = :name")
    suspend fun findByName(name: String): Location?

    @Delete fun delete(location: Location)

    @Query("SELECT name FROM Location")
    fun getAllNames(): LiveData<List<String>>
}