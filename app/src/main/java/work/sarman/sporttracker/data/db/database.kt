package work.sarman.sporttracker.data.db

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters

@Database(entities = [SportActivity::class, Location::class], version = 1)
@TypeConverters(Converters::class)
abstract class SportTrackerRoomDatabase : RoomDatabase() {
    abstract fun sportActivityDao(): SportActivityDao
    abstract fun locationDao(): LocationDao
}