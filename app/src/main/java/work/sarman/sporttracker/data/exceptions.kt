package work.sarman.sporttracker.data

import java.lang.Exception

open class SportTrackerDataException: Exception()

class UserNotLoggedInException : SportTrackerDataException()
class CouldNotGetFirebaseKeyException : SportTrackerDataException()
