package work.sarman.sporttracker

import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.ktx.database
import com.google.firebase.ktx.Firebase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.koin.android.ext.koin.androidApplication
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module
import work.sarman.sporttracker.data.LocationRepository
import work.sarman.sporttracker.data.SportActivityRepository
import work.sarman.sporttracker.data.db.SportTrackerRoomDatabase
import work.sarman.sporttracker.data.firebase.FirebaseDao
import work.sarman.sporttracker.ui.NavigationViewModel
import work.sarman.sporttracker.ui.activities.insert.LocationViewModel
import work.sarman.sporttracker.ui.activities.list.SportActivitiesViewModel

const val ROOM_DATABASE_NAME = "sport_tracker_database"
private val DEFAULT_LOCATIONS = arrayOf("Home", "Gym", "Running track")

val mainModule = module {
    single(createdAtStart = true) {
        Room.databaseBuilder(
            androidApplication(),
            SportTrackerRoomDatabase::class.java,
            ROOM_DATABASE_NAME
        ).addCallback(object : RoomDatabase.Callback() {
            override fun onCreate(db: SupportSQLiteDatabase) {
                super.onCreate(db)

                GlobalScope.launch(Dispatchers.IO) {
                    val locationRepository = get<LocationRepository>()
                    DEFAULT_LOCATIONS.forEach {
                        locationRepository.insert(it)
                    }
                }
            }
        })
            .build()
    }

    single {
        val roomDb: SportTrackerRoomDatabase = get()
        roomDb.sportActivityDao()
    }

    single {
        val roomDatabase: SportTrackerRoomDatabase = get()
        roomDatabase.locationDao()
    }

    single { FirebaseDao(get(), get()) }

    single {
        SportActivityRepository(get(), get(), get(), get())
    }

    single {
        LocationRepository(get())
    }

    factory {
        FirebaseAuth.getInstance()
    }

    factory {
        Firebase.database
    }

    viewModel { SportActivitiesViewModel(get()) }

    viewModel { LocationViewModel(get()) }

    viewModel { NavigationViewModel() }

}