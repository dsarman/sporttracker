package work.sarman.sporttracker.ui

/**
 * Wrapper for data that is exposed via LiveData that represent an event.
 */
class Event<out T>(private val content: T) {
    private var hasBeenHandled = false

    fun getContentIfNotHandled(): T? {
        return if (hasBeenHandled) {
            null
        } else {
            hasBeenHandled = true
            content
        }
    }
}