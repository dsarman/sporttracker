package work.sarman.sporttracker.ui.activities.insert

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import work.sarman.sporttracker.data.LocationRepository

class LocationViewModel(locationRepository: LocationRepository) : ViewModel() {
    val allLocationNames: LiveData<List<String>> = locationRepository.getAllNames()
}