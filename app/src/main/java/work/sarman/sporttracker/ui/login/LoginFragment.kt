package work.sarman.sporttracker.ui.login


import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.firebase.ui.auth.AuthUI
import com.firebase.ui.auth.IdpResponse
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.fragment_login.view.*
import mu.KotlinLogging
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel
import work.sarman.sporttracker.R
import work.sarman.sporttracker.ui.activities.list.SportActivitiesViewModel

private val log = KotlinLogging.logger { }
private const val RC_SIGN_IN = 123

class LoginFragment : Fragment() {

    private val auth by inject<FirebaseAuth>()
    private val activitiesViewModel by viewModel<SportActivitiesViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_login, container, false)
        view.button_login.setOnClickListener {
            launchLoginUi()
        }
        return view
    }

    override fun onResume() {
        super.onResume()
        if (auth.currentUser != null) {
            findNavController().navigate(LoginFragmentDirections.actionNavLoginToNavActivities())
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == RC_SIGN_IN) {
            val idpResponse = IdpResponse.fromResultIntent(data)
            handleLoginResponse(resultCode, idpResponse)
        }
    }

    private fun handleLoginResponse(resultCode: Int, response: IdpResponse?) {
        if (resultCode == Activity.RESULT_OK) {
            activitiesViewModel.refreshFirebaseListener()
            log.info { "Successful login with response '$response'" }
        } else {
            // TODO: Handle back button and no network error responses.
            log.error { "Login error with response '$response'" }
        }
    }

    private fun launchLoginUi() {
        log.debug { "Starting Firebase AuthUI login." }
        startActivityForResult(
            AuthUI.getInstance().createSignInIntentBuilder()
                .setAvailableProviders(listOf(AuthUI.IdpConfig.GoogleBuilder().build()))
                .build(),
            RC_SIGN_IN
        )
    }
}
