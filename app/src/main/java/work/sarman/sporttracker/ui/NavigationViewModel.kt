package work.sarman.sporttracker.ui

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class NavigationViewModel : ViewModel() {
    private val _navigationEvent = MutableLiveData<Event<NavigationCommand>>()

    val navigationEvent: LiveData<Event<NavigationCommand>>
        get() = _navigationEvent

    fun toActivity(id: String) {
        _navigationEvent.postValue(Event(NavigationCommand.ToActivity(id)))
    }
}