package work.sarman.sporttracker.ui.logout


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.GravityCompat
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.firebase.ui.auth.AuthUI
import kotlinx.android.synthetic.main.activity_main.*
import work.sarman.sporttracker.R

/**
 * Fragment with just a progress bar used to add a logout action to a navigation bar set up with
 * a navigation controller.
 */
class LogoutFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_logout, container, false)
    }

    override fun onResume() {
        super.onResume()
        context?.let {
            AuthUI.getInstance()
                .signOut(it)
                .addOnCompleteListener {
                    drawer_layout?.closeDrawer(GravityCompat.START)
                    findNavController().navigate(R.id.nav_activities)
                }
        }
    }
}
