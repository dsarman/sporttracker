package work.sarman.sporttracker.ui.activities.insert


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.fragment_insert_activity.*
import mu.KotlinLogging
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.threeten.bp.Duration
import org.threeten.bp.Instant
import work.sarman.sporttracker.R
import work.sarman.sporttracker.data.SportActivityDTO
import work.sarman.sporttracker.data.SportTrackerDataException
import work.sarman.sporttracker.data.Storage
import work.sarman.sporttracker.ui.activities.list.SportActivitiesViewModel

private val log = KotlinLogging.logger { }

/**
 * Fragment for creating and updating sport activities.
 */
class InsertActivityFragment : Fragment() {

    private val locationViewModel by viewModel<LocationViewModel>()
    private val sportActivityViewModel by viewModel<SportActivitiesViewModel>()
    private val args by navArgs<InsertActivityFragmentArgs>()
    private var isPrefilledFirebase = false

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_insert_activity, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        super.onViewCreated(view, savedInstanceState)
        number_picker_hours.maxValue = 24
        number_picker_hours.minValue = 0

        number_picker_minutes.maxValue = 59
        number_picker_minutes.minValue = 0

        number_picker_seconds.maxValue = 59
        number_picker_seconds.minValue = 0

        button_insert_activity.setOnClickListener {
            saveNewActivity()
        }

        isPrefilledFirebase = false
        prefillFormIfUpdate()
    }

    /**
     * Prefills the form if this is an update of existing activity
     */
    private fun prefillFormIfUpdate() {
        val activityId = args.activityId ?: return
        val activity = sportActivityViewModel.findById(activityId) ?: return
        isPrefilledFirebase = activity.storage == Storage.FIREBASE

        text_edit_activity_name.setText(activity.name)
        autocomplete_activity_location.setText(activity.location)
        number_picker_hours.value = activity.getLengthDisplayHours()
        number_picker_minutes.value = activity.getLengthDisplayMinutes()
        number_picker_seconds.value = activity.getLengthDisplaySeconds()
        switch_remote_storage.visibility = View.GONE
    }

    private fun saveNewActivity() {
        val name = text_edit_activity_name.text.toString()

        var invalid = false
        if (name.isBlank()) {
            text_edit_activity_name.error = getText(R.string.insert_activity_blank_name)
            invalid = true
        }
        if (name.isBlank()) {
            autocomplete_activity_location.error = getText(R.string.insert_activity_blank_location)
            invalid = true
        }
        if (invalid) {
            return
        }

        val secondsDuration =
            number_picker_hours.value * 60 * 60 + number_picker_minutes.value * 60 + number_picker_seconds.value

        val storage = if (switch_remote_storage.isChecked || isPrefilledFirebase) {
            Storage.FIREBASE
        } else {
            Storage.LOCAL
        }

        try {
            val dto = SportActivityDTO(
                id = args.activityId,
                name = text_edit_activity_name.text.toString(),
                length = Duration.ofSeconds(secondsDuration.toLong()),
                createdAt = Instant.now(),
                location = autocomplete_activity_location.text.toString(),
                storage = storage
            )
            if (dto.id == null) {
                sportActivityViewModel.insert(dto)
            } else {
                sportActivityViewModel.update(dto)
            }

            findNavController().navigate(R.id.nav_activities)
        } catch (e: SportTrackerDataException) {
            log.error(e) { "Inserting of new activity failed." }
            view?.let {
                Snackbar.make(it, R.string.insert_error_snack, Snackbar.LENGTH_LONG)
            }
        }
    }

    @Suppress("NAME_SHADOWING")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        locationViewModel.allLocationNames.observe(this, Observer { names ->
            names?.let { names ->
                context?.let { context ->
                    autocomplete_activity_location?.setAdapter(
                        ArrayAdapter<String>(context, android.R.layout.select_dialog_item, names)
                    )
                }
            }
        })

        requireActivity().onBackPressedDispatcher.addCallback(this, object :
            OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                findNavController().navigate(
                    InsertActivityFragmentDirections.actionNavInsertActivityToNavActivities()
                )
            }
        })
    }
}
