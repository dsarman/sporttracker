package work.sarman.sporttracker.ui.activities.list


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.airbnb.epoxy.EpoxyTouchHelper
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import kotlinx.android.synthetic.main.fragment_activities.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import work.sarman.sporttracker.ActivityCardBindingModel_
import work.sarman.sporttracker.R
import work.sarman.sporttracker.activityCard
import work.sarman.sporttracker.data.SportActivityDTO
import work.sarman.sporttracker.ui.NavigationCommand
import work.sarman.sporttracker.ui.NavigationViewModel

/**
 * The main fragment that displays list of all the stored activities.
 */
class ActivitiesFragment : Fragment() {

    private val activitiesViewModel by viewModel<SportActivitiesViewModel>()
    private val navigationViewModel by viewModel<NavigationViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_activities, container, false)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        activitiesViewModel.allActivities.observe(this, Observer { activities ->
            activities?.let {
                updateActivities(it)
            }
        })
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUpSwiping()
        setUpFloatingButton()
        setUpNavigationListener()
    }

    override fun onResume() {
        super.onResume()
        updateActivities()
    }

    private fun setUpFloatingButton() {
        floating_button_go_to_insert_activity?.setOnClickListener {
            findNavController().navigate(R.id.nav_insert_activity)
        }
    }

    private fun setUpSwiping() {
        EpoxyTouchHelper.initSwiping(recycler_view_activities)
            .leftAndRight()
            .withTarget(ActivityCardBindingModel_::class.java)
            .andCallbacks(object : EpoxyTouchHelper.SwipeCallbacks<ActivityCardBindingModel_>() {
                override fun onSwipeCompleted(
                    model: ActivityCardBindingModel_?,
                    itemView: View?,
                    position: Int,
                    direction: Int
                ) {
                    val modelId = model?.activity()?.id ?: return
                    val message =
                        getString(R.string.delete_confirmation_alert_message, model.activity().name)
                    MaterialAlertDialogBuilder(context)
                        .setTitle(R.string.delete_confirmation_alert_title)
                        .setMessage(message)
                        .setCancelable(false)
                        .setPositiveButton(R.string.yes_button) { _, _ ->
                            activitiesViewModel.delete(modelId, model.activity().storage)
                        }.setNegativeButton(R.string.no_button) { _, _ ->
                            updateActivities()
                        }
                        .show()
                }
            })
    }

    /**
     * Listens to navigation events coming from databindings on cards contained in recyclerView
     */
    private fun setUpNavigationListener() {
        navigationViewModel.navigationEvent.observe(this, Observer {
            it?.getContentIfNotHandled()?.let { navigationCommand ->
                when (navigationCommand) {
                    is NavigationCommand.ToActivity -> {
                        val directions = ActivitiesFragmentDirections
                            .actionNavActivitiesToNavInsertActivity(navigationCommand.id)
                        findNavController().navigate(directions)
                    }
                }
            }
        })
    }

    /**
     * Forces redraw of recyclerView.
     * Used when we need to redraw data, but it did not changes (as when swiping a card out, but not deleting it)
     */
    private fun updateActivities() {
        activitiesViewModel.allActivities.value?.let {
            // TODO: find a better solution, since this creates a brief white screen flash
            recycler_view_activities.clear()
            updateActivities(it)
        }
    }

    private fun updateActivities(activities: List<SportActivityDTO>) {
        recycler_view_activities.withModels {
            activities.forEach {
                activityCard {
                    id(it.id)
                    activity(it)
                    navigation(navigationViewModel)
                }
            }
        }
    }
}
