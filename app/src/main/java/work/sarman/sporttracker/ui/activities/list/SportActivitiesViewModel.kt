package work.sarman.sporttracker.ui.activities.list

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import work.sarman.sporttracker.data.SportActivityDTO
import work.sarman.sporttracker.data.SportActivityRepository
import work.sarman.sporttracker.data.Storage

class SportActivitiesViewModel(
    private val sportActivityRepository: SportActivityRepository
) : ViewModel() {
    val allActivities: LiveData<List<SportActivityDTO>> = sportActivityRepository.allActivities

    fun insert(sportActivityDTO: SportActivityDTO) = viewModelScope.launch(Dispatchers.IO) {
        val id = sportActivityRepository.insert(sportActivityDTO)
        sportActivityDTO.id = id
    }

    fun delete(sportActivityId: String, storage: Storage) = viewModelScope.launch(Dispatchers.IO) {
        sportActivityRepository.delete(sportActivityId, storage)
    }

    fun findById(sportActivityId: String): SportActivityDTO? {
        val activities = allActivities.value ?: return null
        for (activity in activities) {
            if (activity.id == sportActivityId) {
                return activity
            }
        }
        return null
    }

    fun update(sportActivityDTO: SportActivityDTO) = viewModelScope.launch(Dispatchers.IO) {
        sportActivityRepository.update(sportActivityDTO)
    }

    /**
     * Used to attach a Firebase database listener after the user sings in.
     */
    fun refreshFirebaseListener() = sportActivityRepository.refreshFirebaseListener()
}