package work.sarman.sporttracker.ui

/**
 * Specifies available navigation commands that can be used as destinations in databinding layouts
 */
sealed class NavigationCommand {
    data class ToActivity(val id: String): NavigationCommand()
}